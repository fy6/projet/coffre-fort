package main

import (
	"flag"
	"fmt"
	"os"

	"exercice.go/safe-psw-manager/safepsw"
)

func main() {

	action := flag.String("action", "list", "Action to performe on the safe")

	s, err := safepsw.New("./bdd")
	handleErr(err)
	defer s.Close()

	/*s.Add("facebook", "sd4g56dg1!#")
	entries, _ := s.List()
	for _, entry := range entries {
		fmt.Println(entries[entry.Key])
	}*/

	/*s.Remove("facebook")
	entries, _ := s.List()
	for _, entry := range entries {
		fmt.Println(entries[entry.Key])
	}*/
	flag.Parse()
	switch *action {
	case "list":
		actionList(s)
	case "add":
		actionAdd(s, flag.Args())
	case "reveal":
		actionReveal(s, flag.Args())
	case "remove":
		actionRemove(s, flag.Args())
	default:
		fmt.Printf("Unknow action: %v\n", *action)
	}

}

func actionAdd(s *safepsw.Safepsw, args []string) {
	key := args[0]
	psw := args[1]
	err := s.Add(key, psw)
	handleErr(err)
	fmt.Printf("Le mot de passe pour '%v', est ajouté au coffre-fort\n", key)
}

func actionReveal(s *safepsw.Safepsw, args []string) {
	key := args[0]
	entry, err := s.Get(key)
	handleErr(err)
	fmt.Println(entry.Psw)
}

func actionRemove(s *safepsw.Safepsw, args []string) {
	key := args[0]
	err := s.Remove(key)
	handleErr(err)
	fmt.Printf("'%v'est retiré du coffre-fort\n", key)
}

func actionList(s *safepsw.Safepsw) {
	entries, err := s.List()
	handleErr(err)
	fmt.Println("Safe content :")
	for _, entry := range entries {
		fmt.Println(entry.Key)
	}
}

func handleErr(err error) {
	if err != nil {
		fmt.Printf("Safe error : %v\n", err)
		os.Exit(1)
	}
}
