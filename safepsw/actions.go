package safepsw

import (
	"bytes"
	"encoding/gob"
	"strings"

	"github.com/dgraph-io/badger"
)

func (s *Safepsw) Add(key string, psw string) error {
	entry := Entry{
		Key: strings.Title(key),
		Psw: psw,
		//Date: time.Now(),
	}

	var buffer bytes.Buffer
	enc := gob.NewEncoder(&buffer)
	err := enc.Encode(entry)
	if err != nil {
		return err
	}

	return s.db.Update(func(txn *badger.Txn) error {
		return txn.Set([]byte(key), buffer.Bytes())
	})
}

func (s *Safepsw) Get(key string) (Entry, error) {
	var entry Entry

	err := s.db.View(func(txn *badger.Txn) error {
		item, err := txn.Get([]byte(key))
		if err != nil {
			return err
		}
		entry, err = getEntry(item)
		return err
	})
	return entry, err
}

func (s *Safepsw) Remove(key string) error {
	return s.db.Update(func(txn *badger.Txn) error {
		return txn.Delete([]byte(key))
	})
}

func (s *Safepsw) List() (map[string]Entry, error) {
	entries := make(map[string]Entry)
	err := s.db.View(func(txn *badger.Txn) error {
		opts := badger.DefaultIteratorOptions
		opts.PrefetchSize = 10
		it := txn.NewIterator(opts)
		defer it.Close()
		for it.Rewind(); it.Valid(); it.Next() {
			item := it.Item()
			entry, err := getEntry(item)
			if err != nil {
				return err
			}
			entries[entry.Key] = entry
		}
		return nil
	})

	return entries, err
}

func getEntry(item *badger.Item) (Entry, error) {
	var entry Entry
	var buffer bytes.Buffer
	item.Value(func(val []byte) error {
		_, err := buffer.Write(val)
		return err
	})

	dec := gob.NewDecoder(&buffer)
	err := dec.Decode(&entry)
	return entry, err
}
