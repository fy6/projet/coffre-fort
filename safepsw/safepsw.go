package safepsw

import (
	"fmt"

	"github.com/dgraph-io/badger"
)

type Safepsw struct {
	db *badger.DB
}

type Entry struct {
	Key string
	Psw string
	//Date time.Time
}

func (e Entry) String() string {

	//created := e.Date.Format(time.Stamp)
	//return fmt.Sprintf("%-10v\t%-30v%-6v", e.Dest, e.Psw, created)
	return fmt.Sprintf("%-20v\t%-50v", e.Key, e.Psw)
}

func New(dir string) (*Safepsw, error) {
	opts := badger.DefaultOptions(dir)
	opts.Dir = dir
	opts.ValueDir = dir
	opts.Logger = nil

	db, err := badger.Open(opts)
	if err != nil {
		return nil, err
	}

	safe := &Safepsw{
		db: db,
	}

	return safe, nil

}

func (s *Safepsw) Close() {
	s.db.Close()
}
